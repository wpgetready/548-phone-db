package db.phone.com.phone_db.classes;

import android.os.AsyncTask;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class DownloadTask extends AsyncTask<String,Void,String> {
    public static String  BASE ="http://phonedb.wpgetready.com/index.php?test=1";

    //This is executed after a successful run of doInBackground, passing the results to here.
    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
    }

    @Override
    protected String doInBackground(String... urls) {
        String result="";
        URL url;
        HttpURLConnection urlConnection = null;
        try {
            url =new URL(urls[0]);
            urlConnection = (HttpURLConnection)url.openConnection();
            InputStream in= urlConnection.getInputStream();
            InputStreamReader reader= new InputStreamReader(in);
            int data=reader.read();
            while (data !=-1) {
                char current =(char)data;
                result +=current;
                data= reader.read();
            }
            return result;

        } catch (MalformedURLException e) {
            e.printStackTrace();
            return "Failed MalformedURLException: " + e.getMessage();
        } catch (IOException e) {
            e.printStackTrace();
            return "Failed IOException: " + e.getMessage();
        }
    }
}
