package db.phone.com.phone_db;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import db.phone.com.phone_db.classes.DownloadTask;
import db.phone.com.phone_db.classes.PhoneSearch;

public class CallService extends AppCompatActivity {

    Button btnSearch;
    TextView txtResult;
    EditText txtSearch;
//Reference:
//https://www.udemy.com/the-complete-android-oreo-developer-course/learn/v4/t/lecture/8339458?start=0

    //READ: https://medium.com/@JasonCromer/android-asynctask-http-request-tutorial-6b429d833e28
//https://loopj.com/android-async-http/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_call_service);
        btnSearch = findViewById(R.id.btnSearch);
        txtSearch = findViewById(R.id.txtSearch);
        txtResult = findViewById(R.id.txtResult);



        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String BASE ="http://phonedb.wpgetready.com/index.php?test=1&search=";
                String s =txtSearch.getText().toString();
                if (s.trim().length()==0){
                    Toast.makeText(CallService.this, "Search EMPTY!", Toast.LENGTH_LONG).show();
                    return;
                }
                String result=null;
                DownloadTask task = new DownloadTask();
                try {
                    result=task.execute(BASE + s).get();

                    //returns -1 when search not found.
                    if (result.equals("-1")) {
                        txtResult.setText("No matches");
                    } else {
                        txtResult.setText(result);
                        Gson gson = new Gson();
                        PhoneSearch[] searchResult = gson.fromJson(result, PhoneSearch[].class);
                    }
                } catch (Exception e){
                    e.printStackTrace();
                }

                Log.i("Result",result);
            }
        });
    }

}
