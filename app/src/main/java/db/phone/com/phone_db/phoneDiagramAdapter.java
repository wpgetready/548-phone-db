package db.phone.com.phone_db;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import db.phone.com.phone_db.classes.PhoneDiagram;

//20190330: viewHolder pattern is a technique for transferring data to controls
// https://www.javacodegeeks.com/2013/09/android-viewholder-pattern-example.html
// or https://www.androidcode.ninja/android-viewholder-pattern-example/

public class phoneDiagramAdapter extends ArrayAdapter<PhoneDiagram> implements View.OnClickListener{

    private ArrayList<PhoneDiagram> dataSet;
    Context mContext;
    private int lastPosition = -1;

    // View lookup cache.
    //ViewHolder contains same objects, same name than activity
    private static class ViewHolder {
        TextView txtFilename;
        TextView txtDescription;
        TextView txtSize;
        ImageView viewDownload;
    }

    public phoneDiagramAdapter(ArrayList<PhoneDiagram> data, Context context) {
        super(context, R.layout.row_item_diagram, data); //Instantiate row
        this.dataSet = data;
        this.mContext=context;
    }

    //20190331:this event is fired when somebody makes click on the icon.
    //Probably it does not have utility in the future
    @Override
    public void onClick(View v) {
        int position=(Integer) v.getTag();
        Object object= getItem(position);
        PhoneDiagram dataModel=(PhoneDiagram)object;
        switch (v.getId())
        {
            case R.id.viewDownload:
                /*
                Snackbar.make(v,  "Phone Id: " +dataModel.getId(), Snackbar.LENGTH_LONG)
                        .setAction("No action", null).show();
                */
                Intent intent = new Intent(mContext,PdfViewer.class);
                intent.putExtra("phoneid",dataModel.getId());
                intent.putExtra("filename",dataModel.getFilename());
                intent.putExtra("sizemb",dataModel.getSizemb());

                mContext.startActivity(intent);
                break;
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position. Pay attention we always have dataModel located
        //All the data is always in the dataModel item.
        PhoneDiagram dataModel = getItem(position);
        // Check if an existing view is being reused, otherwise inflate it
        ViewHolder viewHolder; // view lookup cache stored in tag
        final View result;

        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.row_item_diagram, parent, false);
            viewHolder.txtFilename = (TextView) convertView.findViewById(R.id.txtFilename);
            viewHolder.txtDescription = (TextView) convertView.findViewById(R.id.txtDescription);
            viewHolder.txtSize = (TextView) convertView.findViewById(R.id.txtSize);
            viewHolder.viewDownload = (ImageView) convertView.findViewById(R.id.viewDownload);
            result=convertView;
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
            result=convertView;
        }

        Animation animation = AnimationUtils.loadAnimation(mContext, (position > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
        result.startAnimation(animation);
        lastPosition = position;

        viewHolder.txtFilename.setText(dataModel.getFilename());
        viewHolder.txtDescription.setText(dataModel.getDescription());
        viewHolder.txtSize.setText(String.valueOf(dataModel.getSizemb()));
        viewHolder.viewDownload.setOnClickListener(this); //fires onClick event in this class.
        viewHolder.viewDownload.setTag(position);
        // Return the completed view to render on screen
        return convertView;
    }
}
