package db.phone.com.phone_db;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;

import db.phone.com.phone_db.classes.DownloadTask;
import db.phone.com.phone_db.classes.PhoneDiagram;

public class DiagramActivity extends AppCompatActivity {
    ArrayList<PhoneDiagram> dataModels;
    ListView listView;
    private static phoneDiagramAdapter adapter;

    Button btnBack;
    int phoneid;

    public void searchDiags() {

        String result=null;
        DownloadTask task = new DownloadTask();
        try {
            result=task.execute(DownloadTask.BASE + "&phoneid=" + phoneid).get();

            Gson gson = new Gson();
            PhoneDiagram[] searchResult = gson.fromJson(result, PhoneDiagram[].class);
            dataModels = new ArrayList<PhoneDiagram>(Arrays.asList(searchResult));
            adapter= new phoneDiagramAdapter(dataModels,getApplicationContext());
            listView.setAdapter(adapter);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                    PhoneDiagram dataModel= dataModels.get(position);
                    // https://stackoverflow.com/questions/34432339/android-snackbar-vs-toast-usage-and-difference
                    //Toast and Snackbar are similar and currently for me is almost the same
                    Snackbar.make(view, dataModel.getFilename()+" - "+dataModel.getSizemb()+"MB\nDescription: "+dataModel.getDescription(), Snackbar.LENGTH_LONG)
                            .setAction("No action", null).show();
                }
            });

        } catch (Exception e){
            e.printStackTrace();
        }

        Log.i("Result",result);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_diagram);

        setContentView(R.layout.activity_diagram);
        /*
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
*/

        Intent intent = getIntent();

        phoneid = intent.getIntExtra("phoneid", 0);
        String filename=intent.getStringExtra("filename");
        setTitle("Diagrams for:" + filename);
        listView=(ListView)findViewById(R.id.lstDiagram);
        btnBack = findViewById(R.id.btnBack);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        searchDiags();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}