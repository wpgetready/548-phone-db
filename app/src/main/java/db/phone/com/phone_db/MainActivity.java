package db.phone.com.phone_db;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;

import db.phone.com.phone_db.classes.DownloadTask;
import db.phone.com.phone_db.classes.PhoneSearch;


public class MainActivity extends AppCompatActivity {

    ArrayList<PhoneSearch> dataModels;
    private static phoneSearchAdapter adapter;

    EditText txtSearch;
    Button btnSearch;
    TextView txtLastSearch;
    ListView listView;

    //READ: https://medium.com/@JasonCromer/android-asynctask-http-request-tutorial-6b429d833e28
//https://loopj.com/android-async-http/


    public void searchPhone(View v) {

        String s =txtSearch.getText().toString();
        if (s.trim().length()==0){
            Toast.makeText(MainActivity.this, "Search EMPTY!", Toast.LENGTH_LONG).show();
            return;
        }
        String result=null;
        DownloadTask task = new DownloadTask();
        try {
            result=task.execute(DownloadTask.BASE + "&search=" + s).get();

            //returns -1 when search not found.
            if (result.equals("-1")) {
                txtLastSearch.setText("No matches");
                listView.setVisibility(View.GONE);
            } else {
                //txtResult.setText(result);
                listView.setVisibility(View.VISIBLE);
                txtLastSearch.setText(txtSearch.getText());
                Gson gson = new Gson();
                PhoneSearch[] searchResult = gson.fromJson(result, PhoneSearch[].class);
                dataModels = new ArrayList<PhoneSearch>(Arrays.asList(searchResult));
                adapter= new phoneSearchAdapter(dataModels,getApplicationContext());
                listView.setAdapter(adapter);
                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                        PhoneSearch dataModel= dataModels.get(position);
                        // https://stackoverflow.com/questions/34432339/android-snackbar-vs-toast-usage-and-difference
                        //Toast and Snackbar are similar and currently for me is almost the same
                        Snackbar.make(view, dataModel.getBrand()+" - "+dataModel.getModel()+"\nName: "+dataModel.getName(), Snackbar.LENGTH_LONG)
                                .setAction("No action", null).show();
                    }
                });
            }
        } catch (Exception e){
            e.printStackTrace();
        }

        Log.i("Result",result);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        listView=(ListView)findViewById(R.id.list);
        txtSearch = findViewById(R.id.txtSearchMain);
        txtLastSearch = findViewById(R.id.txtLastSearch);

        btnSearch = findViewById(R.id.btnS);
        View.OnClickListener btn_click = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchPhone(v);
            }
        };
        btnSearch.setOnClickListener(btn_click);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}



    /*
    How to open a pdf using pdfView, working example
    // https://www.youtube.com/watch?v=-Ld1IoOF_uk
    PDFView pdfView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
     pdfView = (PDFView) findViewById(R.id.pdfView);

     PDFView.Configurator c = pdfView.fromAsset("I9060.pdf");
     c.swipeHorizontal(true);
     c.pageSnap(true);
     c.autoSpacing(true);
     c.pageFling(true);
     c.load();
    }
*/