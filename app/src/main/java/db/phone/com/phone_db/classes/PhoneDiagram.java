package db.phone.com.phone_db.classes;
/*

Diagrams for a phone
http://phonedb.wpgetready.com/index.php?test=1&phoneid=1

[{"id":"1","id_phone":"1","filename":"I9060-cover.pdf","description":null,"format":"pdf","sizemb":"0"},
{"id":"2","id_phone":"1","filename":"I9060-disassembly-reassembly.pdf","description":"Schematics repair with pictures","format":"pdf","sizemb":"0"},
{"id":"3","id_phone":"1","filename":"I9060-electrical-part-list.pdf","description":"Warning: part list and electrical part list are different","format":"pf","sizemb":"0"},
{"id":"4","id_phone":"1","filename":"I9060-exploded-view.pdf","description":null,"format":"pdf","sizemb":"0"},
{"id":"5","id_phone":"1","filename":"I9060-part-list.pdf","description":"Warning: part list and electrical part list are different","format":"pdf","sizemb":"0"},
{"id":"6","id_phone":"1","filename":"I9060-pcb-diagrams.pdf","description":"Level Repair","format":"pdf","sizemb":"0"}]
 */
public class PhoneDiagram {

    private int id;
    private int id_phone;
    private String filename;
    private String description;
    private String format;
    private float sizemb;

    public PhoneDiagram(int id, int id_phone, String filename, String description, String format, float sizeMb){
        this.id=id;
        this.id_phone =id_phone;
        this.filename=filename;
        this.description =description;
        this.format = format;
        this.sizemb =sizeMb;
    }
    public int getId() {
        return id;
    }
    public String getFilename() {
        return filename;
    }
    public String getDescription() {
        return description;
    }
    public String getFormat() { return format; }
    public float getSizemb() {  return sizemb;  }
}

/*
    public void setId(int id) {
        this.id = id;
    }
    public void setSizemb(float sizemb) {
        this.sizemb = sizemb;
    }
    public void setId_phone(int id_phone) {
        this.id_phone = id_phone;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }
    */
