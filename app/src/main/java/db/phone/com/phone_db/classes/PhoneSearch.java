package db.phone.com.phone_db.classes;

/**
 * Created by anupamchugh on 09/02/16.
 * [{"id":"1","name":"GT-I9060I Galaxy Grand Neo Plus Duos","id_brand":"85","brand":"Samsung","model":"I9060"},
 * {"id":"2","name":"SM-J710FN DS Galaxy J7 2016 Duos 4g LTE","id_brand":"85","brand":"Samsung","model":"SM-J710FN"}]
 */

public class PhoneSearch {
    private int id;
    private String name;
    private int brand_name;
    private String brand;
    private String model;

    public PhoneSearch(int id, String name, int brand_name, String brand, String model) {
        this.id=id;
        this.name=name;
        this.brand_name=brand_name;
        this.brand=brand;
        this.model=model;
    }

    public int getId() {
        return id;
    }
    public String getName() {
        return name;
    }
    public int getBrand_name() {
        return brand_name;
    }
    public String getBrand() {
        return brand;
    }
    public String getModel() {
        return model;
    }

}

