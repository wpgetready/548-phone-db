package db.phone.com.phone_db;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Base64OutputStream;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.github.barteksc.pdfviewer.PDFView;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

import db.phone.com.phone_db.classes.DownloadTask;


public class PdfViewer extends AppCompatActivity {

    ByteArrayOutputStream output;
    Button btnBack;
    PDFView pdfView;
    String TEST_URL="http://phonedb.wpgetready.com/index.php?test=1&diagramid=11";

    public static final int progress_bar_type = 0;
    private ProgressDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pdfview);

        Intent intent = getIntent();
        int phoneid = intent.getIntExtra("phoneid", 0);
        float sizemb = intent.getFloatExtra("sizemb",0);

        initPDF(phoneid,sizemb);

        btnBack = findViewById(R.id.btnPdfBack);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case progress_bar_type:
                pDialog = new ProgressDialog(this);
                pDialog.setMessage("Downloading file. Please wait...");
                pDialog.setIndeterminate(false);
                pDialog.setMax(100);
                pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                pDialog.setCancelable(true);
                pDialog.show();
                return pDialog;
            default:
                return null;
        }
    }

    private void initPDF(int phoneid, float sizemb){
        new DownloadFileFromURL().execute(DownloadTask.BASE +"&diagramid=" +phoneid, String.valueOf(sizemb));
    }

    class DownloadFileFromURL extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute(){
            super.onPreExecute();
            showDialog(progress_bar_type);
        }

        @Override
        protected  String doInBackground(String... f_url){
            int count;
            try{
                URL url = new URL(f_url[0]);
                URLConnection connection = url.openConnection();
                connection.connect();

                int lenghtOfFile = (int) Float.parseFloat( f_url[1]) *1024 *1024 ;
                //MAKE SURE ALL FILES HAVE SIZEMB or a default value
                if (lenghtOfFile ==0 ) {
                    lenghtOfFile =10*1024*1024;
                }


                InputStream input = new BufferedInputStream(url.openStream(), 8192);
/*
                String storageDir = Environment.getExternalStorageDirectory().getAbsolutePath();
                String fileName = "/downloadedfile.jpg";
                File imageFile = new File(storageDir+fileName);
                */
                //  OutputStream output_old = new FileOutputStream(imageFile);
                output = new ByteArrayOutputStream();

                byte data[] = new byte[1024];
                long total = 0;

                while((count = input.read(data)) != -1){
                    total += count;

                    publishProgress(""+(int)((total*100)/lenghtOfFile));
                    Log.i("Data",String.valueOf((int)((total*100)/lenghtOfFile)));
                    output.write(data, 0, count);
                }
                output.flush();

                //output.close();
                input.close();
            }catch (Exception e){
                Log.e("Error: ", e.getMessage());
            }

            return null;

        }

        protected void onProgressUpdate(String... progress){
            pDialog.setProgress(Integer.parseInt(progress[0]));
        }

        @Override
        protected void onPostExecute(String file_url){
            dismissDialog(progress_bar_type);
            pdfView =  (PDFView) findViewById(R.id.pdfView);
            byte[] decodedPdf = Base64.decode( output.toByteArray(), Base64.DEFAULT);

            //PDFView.Configurator c = pdfView.fromAsset("I9060.pdf");
            PDFView.Configurator c = pdfView.fromBytes(decodedPdf);
            c.swipeHorizontal(true);
            c.pageSnap(true);
            c.autoSpacing(true);
            c.pageFling(true);
            c.load();

          //  String imagePath = Environment.getExternalStorageDirectory() + "/downloadedfile.jpg";
           // my_image.setImageDrawable(Drawable.createFromPath(imagePath));
        }
    }
}


    /*
    How to open a pdf using pdfView, working example
    // https://www.youtube.com/watch?v=-Ld1IoOF_uk
    PDFView pdfView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
     pdfView = (PDFView) findViewById(R.id.pdfView);

     PDFView.Configurator c = pdfView.fromAsset("I9060.pdf");
     c.swipeHorizontal(true);
     c.pageSnap(true);
     c.autoSpacing(true);
     c.pageFling(true);
     c.load();
    }
    */