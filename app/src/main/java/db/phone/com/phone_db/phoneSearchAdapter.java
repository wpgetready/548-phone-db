package db.phone.com.phone_db;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import db.phone.com.phone_db.classes.PhoneSearch;

public class phoneSearchAdapter extends ArrayAdapter<PhoneSearch> implements View.OnClickListener{

    private ArrayList<PhoneSearch> dataSet;
    Context mContext;

    // View lookup cache
    private static class ViewHolder {
        TextView txtBrand;
        TextView txtModel;
        TextView txtName;
        ImageView info;
    }

    public phoneSearchAdapter(ArrayList<PhoneSearch> data, Context context) {
        super(context, R.layout.row_item_phone, data); //Instantiate row
        this.dataSet = data;
        this.mContext=context;
    }

    //this event is fired when somebody makes click on the icon.
    @Override
    public void onClick(View v) {
        int position=(Integer) v.getTag();
        Object object= getItem(position);
        PhoneSearch dataModel=(PhoneSearch)object;
        switch (v.getId())
        {
            case R.id.item_info:
              /*
                Snackbar.make(v, "Phone Id: " +dataModel.getId(), Snackbar.LENGTH_LONG)
                        .setAction("No action", null).show();
               */
              //mContext.startActivity(new Intent(mContext,DiagramActivity.class);
              Intent intent = new Intent(mContext,DiagramActivity.class);
              intent.putExtra("phoneid",dataModel.getId());
              intent.putExtra("phonename",dataModel.getName());
                Log.i("ZZZ","Phone name: " + dataModel.getName());
              mContext.startActivity(intent);
                break;
        }
    }

    private int lastPosition = -1;

    //20190330: viewHolder pattern is a technique for transferring data to controls
    // https://www.javacodegeeks.com/2013/09/android-viewholder-pattern-example.html
    // or https://www.androidcode.ninja/android-viewholder-pattern-example/
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position. Pay attention we always have dataModel located
        //There is no need to pass data to no place, all the data is always in the dataModel item.
        PhoneSearch dataModel = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        ViewHolder viewHolder; // view lookup cache stored in tag

        final View result;

        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.row_item_phone, parent, false);
            viewHolder.txtName = (TextView) convertView.findViewById(R.id.txtBrand);
            viewHolder.txtModel = (TextView) convertView.findViewById(R.id.txtModel);
            viewHolder.txtName = (TextView) convertView.findViewById(R.id.txtName);
            viewHolder.info = (ImageView) convertView.findViewById(R.id.item_info);
            result=convertView;
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
            result=convertView;
        }

        Animation animation = AnimationUtils.loadAnimation(mContext, (position > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
        result.startAnimation(animation);
        lastPosition = position;


        viewHolder.txtName.setText(dataModel.getName());
        viewHolder.txtModel.setText(dataModel.getModel());
        viewHolder.txtName.setText(dataModel.getName());
        viewHolder.info.setOnClickListener(this); //fires onClick event in this class.
        viewHolder.info.setTag(position);
        // Return the completed view to render on screen
        return convertView;
    }
}
